import { useForm } from "react-hook-form";

const translateValues = {
        required: true,
        minLength: 2,
        maxlength: 25
}

const TranslationForm = () => {

    const { register, handleSubmit } = useForm()

    const onSubmit = data => {
        console.log(data)
    }

    return (
        <form onSubmit={ handleSubmit(onSubmit) }>
            <fieldset>
                <label htmlFor="translation-notes">Translation: </label>
                <input type="text"{...register('translationNotes', translateValues)} />
            </fieldset>

            <button type="submit">Translate</button>
        </form>
    )

} 
export default TranslationForm
