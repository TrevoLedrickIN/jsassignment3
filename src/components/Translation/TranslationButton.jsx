const TranslationButton = ({ translations }) => {
    const translationImg = translations.map( 
        (letter, index) => {
            if(letter.match(/[\w]/)){
                return <img key={index} src={`LostInTranslation_Ressources/individual_signs/${letter.toLowerCase()}.png`} widt="55" alt={letter}/>
            }
            else {
                return <p> </p>
            }
        }
    )

    return (
        <ul>
            { translationImg }
        </ul>
    )
}

export default TranslationButton