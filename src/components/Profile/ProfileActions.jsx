import { Link } from "react-router-dom"
import { translationClearHistory } from "../../api/translation"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { useUser } from "../../context/UserContext"
import { storageDelete, storageSave } from "../../utils/storage"

const ProfileActions = () => {

    const { user, setUser } = useUser()

    const handleLogoutClick = () => {
        if (window.confirm('Are you sure?')){
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }

    // Gives a 404 NotFound error, dont know why.
    const handleClearHistoryClick = async () => {
        if (!window.confirm('Are you sure? \n This can not be undone!')){
            return
        }

        const [ clearError ] = await translationClearHistory(user)

        if(clearError !== null){
            return
        }

        const updateUser = ({
            ...user,
            translation: []
        })

        storageSave(updateUser)
        setUser(updateUser)
    }

    return (
        <ul>
            <li><Link to="/translation">Translation</Link></li>
            <button onClick={ handleClearHistoryClick }>Clear history</button>
            <br />
            <button onClick={ handleLogoutClick }>Logout</button>
        </ul>
    )
}

export default ProfileActions
