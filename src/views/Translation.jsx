import TranslationButton from "../components/Translation/TranslationButton"
import TranslationForm from "../components/Translation/TranslationForm"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"
import '../css/translation.css'

const Translation = () => {

    const { user } = useUser()

    return (
        <>
            <h1>Translation</h1>
            <section id="translation-notes">
                <TranslationForm />
                <TranslationButton translations={user.translations} />
            </section>
        </>
    )
}

export default withAuth(Translation)