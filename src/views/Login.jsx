import LoginForm from "../components/Login/LoginForm"
import '../App.css'

const Login = () => {
    return (
        <>
            <h1>Login</h1>
            <LoginForm />
        </>
    )
}
export default Login